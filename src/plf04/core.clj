(ns plf04.core)

;; 1.- StringE
(defn string-e-1  ;; recursividad lineal.
  [s]
  ( letfn [(f [s x]
              (if (zero? (count s))
                (if (and (>= x 1) (<= x 3))
                  true 
                  false)
                (if (zero? (compare (subs s (dec (count s)) (count s)) "e"))
                  (f (subs s 0 (dec (count s))) (inc x))
                  (f (subs s 0 (dec (count s))) x))))]
   (f s 0)))

(string-e-1 "Hello")  ;; Pruebas hechas a la función recientemente
(string-e-1 "Heelle")  ;; definida con **TODAS** las pruebas
(string-e-1 "Heelele")  ;; ofrecidas en CodignBat para dicho problema.

(defn string-e-2 
  [...]
  (
   ;; recursividad de cola.
   ))

(string-e-2 ...)  ;; Pruebas hechas a la función recientemente
(string-e-2 ...)  ;; definida con **TODAS** las pruebas
(string-e-2 ...)  ;; ofrecidas en CodignBat para dicho problema.


;;2.- StringTimes
(defn string-times-1
  [s n]
  (letfn [(f [s n]
             (if (zero? (dec n))
               s
               (str s (f s (dec n)))
                   ))]
   (f s n)))

(string-times-1 "Hi" 2)  ;; Pruebas hechas a la función recientemente
(string-times-1 "Hi" 3)  ;; definida con **TODAS** las pruebas
(string-times-1 "Hi" 1)  ;; ofrecidas en CodignBat para dicho problema.

(defn string-times-2
  [...]
  (;; recursividad de cola.
   ))

(string-times-2 ...)  ;; Pruebas hechas a la función recientemente
(string-times-2 ...)  ;; definida con **TODAS** las pruebas
(string-times-2 ...)  ;; ofrecidas en CodignBat para dicho problema.


;;3.- FrontTimes
(defn front-times-1
  [s n]
  (letfn [(f[s n]
           (if (zero? (dec n))
             s
             (str (subs s 0 3) (f (subs s 0 3) (dec n)))
                  ))]
   (f s n)))

(front-times-1 "Chocolate" 2)  ;; Pruebas hechas a la función recientemente
(front-times-1 "Chocolate" 3)  ;; definida con **TODAS** las pruebas
(front-times-1 "Abc" 3)  ;; ofrecidas en CodignBat para dicho problema.

(defn front-times-2
  [...]
  (;; recursividad de cola.
   ))

(front-times-2 ...)  ;; Pruebas hechas a la función recientemente
(front-times-2 ...)  ;; definida con **TODAS** las pruebas
(front-times-2 ...)  ;; ofrecidas en CodignBat para dicho problema.


;;4.- CountXX
(defn count-xx-1
  [s]
  (letfn [(f [s x]
            (if (< (count s) 2)
              x
              (if (zero? (compare (subs s 0 2) "xx"))
                (f (subs s 1 (count s) ) (inc x))
                (f (subs s 1 (count s) ) x))))]
(f s 0)))

(count-xx-1 "abcxx")  ;; Pruebas hechas a la función recientemente
(count-xx-1 "xxx")  ;; definida con **TODAS** las pruebas
(count-xx-1 "xxxx")  ;; ofrecidas en CodignBat para dicho problema.

(defn count-xx-2
  [...]
  (;; recursividad de cola.
   ))

(count-xx-2 ...)  ;; Pruebas hechas a la función recientemente
(count-xx-2 ...)  ;; definida con **TODAS** las pruebas
(count-xx-2 ...)  ;; ofrecidas en CodignBat para dicho problema.


;;5.- stringSplosion
(defn string-splosion-1
  [s]
  (letfn [(f [s x]
            (if (== (dec (count s)) x)
              s
              (str (subs s 0 (inc x))(f s (inc x)))))]
(f s 0)))

(string-splosion-1 "Code")  ;; Pruebas hechas a la función recientemente
(string-splosion-1 "abc")  ;; definida con **TODAS** las pruebas
(string-splosion-1 "ab")  ;; ofrecidas en CodignBat para dicho problema.

(defn string-splosion-2
  [...]
  (;; recursividad de cola.
   ))

(string-splosion-2 ...)  ;; Pruebas hechas a la función recientemente
(string-splosion-2 ...)  ;; definida con **TODAS** las pruebas
(string-splosion-2 ...)  ;; ofrecidas en CodignBat para dicho problema.


;;6.- array123
(defn array-123-1
  [x]
  (letfn [(f [x c]
             (if (> c 0)
               true
               (if (>= (count x) 3)
                (if (and (== (get x (- (count x) 3)) 1) (== (get x (- (count x) 2)) 2) (== (get x (- (count x) 1)) 3))
                  (f (pop x) (+ c 1))
                  (f (pop x) c))
                 false)))]
   (f x 0)))

(array-123-1 [1, 1, 2, 3, 1])  ;; Pruebas hechas a la función recientemente
(array-123-1 [1, 1, 2, 5, 1])  ;; definida con **TODAS** las pruebas
(array-123-1 [1, 2, 3, 4, 2, 3])  ;; ofrecidas en CodignBat para dicho problema.

(defn array-123-2
  [...]
  (;; recursividad de cola.
   ))

(array-123-2 ...)  ;; Pruebas hechas a la función recientemente
(array-123-2 ...)  ;; definida con **TODAS** las pruebas
(array-123-2 ...)  ;; ofrecidas en CodignBat para dicho problema.


;;7.- stringX
(defn string-x-1
  [s]
  (letfn [(f [s x]
            (if (== (dec (count s)) x)
              s
              (if-not (zero? (compare (subs s (inc x) (inc x)) "x"))
                (str (subs s 0 (dec x)) (f (subs s (inc x) (dec (count s))) (inc x)))
                (f (subs s (inc x) (count s)) (inc x)))))]
(f s 0)))

(string-x-1 "abHdia")  ;; Pruebas hechas a la función recientemente
(string-x-1 "abxxxcd")  ;; definida con **TODAS** las pruebas
(string-x-1 "xabxxxcdx")  ;; ofrecidas en CodignBat para dicho problema.

(defn string-x-2
  [...]
  (;; recursividad de cola.
   ))

(string-x-2 ...)  ;; Pruebas hechas a la función recientemente
(string-x-2 ...)  ;; definida con **TODAS** las pruebas
(string-x-2 ...)  ;; ofrecidas en CodignBat para dicho problema.

;;8.- altPairs
(defn alt-pairs-1
  [s]
  (letfn [(f [s]
             (if (< (count s) 4)
               s
               (str (subs s 0 2) (f (subs s 4 (count s))))
                 ))]
   (f s)))

(alt-pairs-1 "kitten")  ;; Pruebas hechas a la función recientemente
(alt-pairs-1 "Chocolate")  ;; definida con **TODAS** las pruebas
(alt-pairs-1 "CodingHorror")  ;; ofrecidas en CodignBat para dicho problema.

(defn alt-pairs-2
  [...]
  (;; recursividad de cola.
   ))

(alt-pairs-2 ...)  ;; Pruebas hechas a la función recientemente
(alt-pairs-2 ...)  ;; definida con **TODAS** las pruebas
(alt-pairs-2 ...)  ;; ofrecidas en CodignBat para dicho problema.


;;9.- stringYak
(defn string-yak-1
  [...]
  (;; recursividad lineal.
   ))

(string-yak-1 ...)  ;; Pruebas hechas a la función recientemente
(string-yak-1 ...)  ;; definida con **TODAS** las pruebas
(string-yak-1 ...)  ;; ofrecidas en CodignBat para dicho problema.

(defn string-yak-2
  [...]
  (;; recursividad de cola.
   ))

(string-yak-2 ...)  ;; Pruebas hechas a la función recientemente
(string-yak-2 ...)  ;; definida con **TODAS** las pruebas
(string-yak-2 ...)  ;; ofrecidas en CodignBat para dicho problema.


;;10.- has271
(defn has-271-1
  [...]
  (;; recursividad lineal.
   ))

(has-271-1 ...)  ;; Pruebas hechas a la función recientemente
(has-271-1 ...)  ;; definida con **TODAS** las pruebas
(has-271-1 ...)  ;; ofrecidas en CodignBat para dicho problema.

(defn has-271-2
  [...]
  (;; recursividad de cola.
   ))

(has-271-2 ...)  ;; Pruebas hechas a la función recientemente
(has-271-2 ...)  ;; definida con **TODAS** las pruebas
(has-271-2 ...)  ;; ofrecidas en CodignBat para dicho problema.